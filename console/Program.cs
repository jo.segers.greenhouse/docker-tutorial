﻿using System;

namespace console {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine(@"This is my console application. There are many like it, but this one is mine.
My console application is my best friend.It is my life. I must master it as I must master my life.
Without me, my console application is useless. Without my console application, I am useless.");
        }
    }
}
